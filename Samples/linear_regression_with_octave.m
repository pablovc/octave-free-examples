% EJERCICIO DESCIRTO DISPONIBLE EN
% https://www.lauradhamilton.com/tutorial-linear-regression-with-octave
% Load the data from our text file
data = load('table.txt');

% Define x and y
x = data(:,2);
y = data(:,1);


% Plot the data

plotData(x,y);

% Count how many data points we have
m = length(x);
% Add a column of all ones (intercept term) to x
X = [ones(m, 1) x];
% Calculate theta
theta = (pinv(X'*X))*X'*y
% Plot the fitted equation we got from the regression
hold on; % this keeps our previous plot of the training data visible
plot(X(:,2), X*theta, '-')
%legend('Training data', 'Linear regression')
title ("Recta ajustada a los puntos");
hold off % Don't put any more plots on this figure

% De la salida de Theta se tiene el modelo theta=[b,m]
%Reinsertando en la ecuacion de la recta queda como 
%y=mx+b, donde m es la pendiente y b es la ordenada al origen 
%recordar que en el llenado de la tabla primero se ingresan los valores de "y"

% Create a function to plot the data
function plotData(x,y)

plot(x,y,'rx','MarkerSize',8); % Plot the data
hold on;
title ("Dispersion de puntos");
xlabel('Variable en X'); % Set the x-axis label
ylabel('Variable en Y'); % Set the y-axis label

hold off;
end
