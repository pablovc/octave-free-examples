printf("Perfil de onda e .^(-x.^2)\n\n");
x = -0:1:20;
   %plot(x,  e .^(-x.^2))
   plot(x,  e .^(-(x-10).^2))
   %Con referencia a la primera impresiOn se introduce un (-10)
   %representa la multiplicaciOn de la velocidad y el tiempo
   %en analísis dimensional representa la distancia
   % y es la distancia en un tiempo "t" de la onda de su
   % posisción inicial 
  hold on;
  xlabel('Variable en X'); % Set the x-axis label
  ylabel('Variable en Y'); % Set the y-axis label
  title ("Perfil de onda 1");
  hold off;
  
  printf("Perfil de onda 3./((10*(x.^2))+1)\n\n");
x = 0:0.1:8;
   %plot(x,  e .^(-x.^2))
   plot(x,  3./((10*(x.^2))+1)); hold on;
    plot(x,  3./((10*((x-1).^2))+1)); hold on;
    plot(x,  3./((10*((x-2).^2))+1)); hold on;
     plot(x,  3./((10*((x-3).^2))+1)); hold on;
 %ejemplo del libro de HECHT pag 21 velocidad de la onda 1[m/s]
  hold on;
  xlabel('Variable en X'); % Set the x-axis label
  ylabel('Variable en Y'); % Set the y-axis label
  title ("Perfil de onda 2");
  hold off;