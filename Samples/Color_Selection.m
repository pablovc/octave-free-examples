%Funcion permitiendo graficar con colores adecuados

function Color = Color_Selection(X)

switch X
    
    case 1
        Color = '-k';
        
    case 2
        Color = '-b';
        
    case 3
        Color = '-g';
        
    case 4
        Color = '-r';
end
end