%***CONSTANTES
%Velocidad de la luz en el vacIo [m/s]
c_0=299792458;
%Constante de Plank [Js]
h=6.62607015*(10.^-34);
%Constante de la gravitaciOn Universal [\frac{Nm^2}{Kg^2}]
G=6.67384*(10.^-11);
%Permitividad elEctrica del vacIo [\frac{C^2}{Nm^2}]
ep_0=8.8541878176*(10.^-12);
%Permitividad magnEtica del vacIo [\frac{Tm}{A}]
mu_0=4*pi*(10.^-7);
%Carga electron [C]
elec=1.602176565*(10.^-19);


%****Funciones matematicas

printf("raices\n");
sqrt(2)
cbrt(2) %raiz cúbica
nthroot(2,3) %raiz a la "n"
nthroot(2,4) %raiz a la "n"

printf("logaritmos\n");
log(3)%logaritmo natural de 3
log2(3)%logaritmo base 2 de 3
log10(3)%logaritmo base 10 de 3

x=10;
y=20;

r=sqrt(x.^2+y.^2)
%conversion de radianes en grados
%si se quiere dejar en grados se debe omitir la conversiOn
theta=atan(y/x)*deg

z_1=3+4i
z_2=3+2i
printf("radio\n");
abs(z_1)%tambien usado para el valor absoluto

printf("argumento\n");
%arg(z_1)*deg
angle(z_1)*deg

printf("conjugado\n");
conj(z_1)

%se pueden realizar operaciones con numeros complejos +,-,*,/
z_1/z_2

%factorial
factorial(3)

%Integral definida
p_0 = [1, 0, 1];%polinomio f(x)=x^2+1 
integral_0 = polyint (p_0);
area_0 = polyval (integral_0, 3) - polyval (integral_0, 0)

%Derivada definida
derivada_0=polyder(p_0);
valuacion_0=polyval(derivada_0,2)

%Raices de un polinomio
roots(p_0)

%Suma de 1 en 1. De 1 a 5
sum([1:5])

%Suma de exp(-x). De 1 a 5
func = @(x) exp(-x);
sum(func([1:5]))


%****Ecuaciones Simultaneas****

%Ejemplo disponible en
%https://homepages.math.uic.edu/~hanson/Octave/OctaveLinearAlgebra.html
% Sample Input of 4X4 Matrix with Rows Separated by Semicolons
%  and elements in each row Separated by Commas:
%A=[0,2,0,1;2,2,3,2;4,-3,0,1.;6,1,-6,-5]

A=[0,2+i,0,1;2+3i,2,3-4i,2;4,-3,0,1.;6,1,-6,-5]

% Sample Input of Column Vector for Matrix Multiplication:
B=[0;-2;-7;6]

% Matrix Solve is A\B in Octave for A^(-1)*B in Math:
%     (Back Slash (\) ALERT: "\" for "divided into")
solucion=A\B

%ConversiondeBases

%ejemplos encontrados en 
%https://octave.org/doc/v4.2.0/String-Conversions.html

hex2dec ("FF")
bin2dec ("1110")
dec2bin (14)
dec2hex (2748)
dec2base (123, 3)
dec2base (123, "aei")
%Convert s from a string of digits in base base to a decimal integer (base 10). 
base2dec ("11120", 3)

printf("Example 'plot'\n\n");
x = 1:0.1:10;
  plot(x, sin(x))
  hold on;
  xlabel('Variable en X'); % Set the x-axis label
  ylabel('Variable en Y'); % Set the y-axis label
  title ("Simple 2-D Plot");
  hold off;

 colormap ("default");
 [X, Y] = meshgrid (linspace (-1, 1, 100));
 Z = real(sqrt(1-X.^2-Y.^2));
 %mesh (X, Y, Z);
 meshc (X, Y, Z)
  %meshz(X, Y, Z);
  %waterfall(Z)
  %contour(X,Y,Z); 
  %scatter(X,Y,Z);%Scatter realiza el mapeo desde arriba
 %surf(Z)
 
 hold on;
 %axis([-1,1,-1,1,0,0.9]);
 title ("3-D Plot");
hold off;
 

   
    function x_dot = f (x, t) 
 
       r = 0.15;
       k = 1.6;
       a = 1.25;
       b = 0.12;
       c = 0.89;
       d = 0.58;
   
       x_dot(1) = r*x(1)*(1 - x(1)/k) - a*x(1)*x(2)/(1 + b*x(1));
       x_dot(2) = c*a*x(1)*x(2)/(1 + b*x(1)) - d*x(2);
    
    end
 
    x = lsode ("f", [1; 2], (t = linspace (0, 50, 200)'));
    
     plot (t, x)
 
 
 %syms n
 %m=(n.^2)*cos(n)
 
34*3