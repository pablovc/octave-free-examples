%Funcion permitiendo el calculo del sistema de ecuaciones diferenciales

function Diff = Diff_System (~, X)
% Constantes que son necesarias para los balances de materia y energia para la obtencion de los perfiles de
%concentracion y temperatura.
C_reactive = X(1); %Concentracion del reactivo que es variable con respecto al tiempo debido a su consumo.
T = X(2); %Temperatura a la que debe ocurrir la reaccion para que se llegue a un consumo.
V = 18e-3; %m3 Volumen del reactor y de la solucion
R = 8.314; %J/kmol*K Constante de los gases ideales
Hr = -2.09e8; %J/KG*K Calor de reaccion
Den = 1000; %Kg/m3 Densidad de la solucion
Cp = 4.19; %kj/kg*K Capacidad calorifica especifica del medio de reaccion
C_entrada = 3; % Kmol/m3 Concentracion de entrada
T_entrada = 298;  % K (kelvin) Temperatura de entrada
Q = 6.0e-6; % m3/seg Flujo volumetrico
r_1 = -4.48e6*C_reactive*exp(-62800/(R*T)); % kmol/m3*s Velocidad de reaccion

%Aqui se observan los balances de materia y energia que deben solucionarse simultaneamente, debido a que en el factor
%r_1 que es la velocidad de reaccion y se ve involucrada en ambos balaneces depende de la temperatura (T) y de la
%concentracion (C_reactive) que son las dos variables.

dCrdt = (Q/V)*C_entrada - (Q/V)*C_reactive + r_1; % Balance de materia 

dTdt = (Q/V)*(T_entrada - T) + (-Hr/1000)*(-r_1)*V/(Den*V*Cp); % Balance de energ�a. /1000 es por conversi�n unidades.


Diff = [dCrdt; dTdt]; %Identificacion de las dos ecuaciones diferenciales de los balances de materia y energia.

end

