clc
clear
%UNIVERSIDAD NACIONAL DE COLOMBIA
%FACULTAD DE MAESTRIA EN INGENIERIA QUIMICA Y AMBIENTAL

%Metodos Matematicos y Numericos en Ingenieria Ambiental
%Octave 4.0
%Perfiles de concentracion y temperatura para una reaccion irreversible homogenea que se da en un reactor que 
%trabaja adiabaticamente.
%Estudiantes:   Ing. Paula Meneses
%               Ing. Fabian Rojas
%               Est. Arnaud Niesz
%Profesor:      Msc. Luis Belalcazar

%RESUMEN: El programa actual se realiza con el fin de obtener perfiles de concentracion y temperatura con respecto
%al tiempo, teniendo en cuenta que la operacion del reactor es adiabatica y presion constante. Para los perfiles
%se tienen condiciones iniciales diferentes: 3 condiciones iniciales para concentracion y 4 para temperatura. Para
%la resolucion de este problema se deben tener los balances de masa y energia los cuales deben ser resueltos
%simultaneamente por el metodo de runge kutta de 4° orden y la funcion de octave que se ajusta perfectamente es
%ODE45.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Control = 1000; % define la precisión del vector tiempo
C_inicial = [0, 1, 2]; % concentraciones iniciales

T_inicial = [298, 308, 318, 328]; % temperaturas iniciales

t_span = linspace (0,5*3600,Control); %El codigo se desarollara para "Control" elementos 
                                      %igualmente espaciados entre 0 y 5 h y expresados en segundos
	
t = zeros(Control,1,size(T_inicial,2),size(C_inicial,2));
C_reactive = zeros(Control,2,size(T_inicial,2),size(C_inicial,2));
%Vectores de tiempo y concentración, de 4 dimensiones, permitiendo el calculo para cada valor de 
%control y cada conjunto de temperatura y concentracion inicial
   
%Loop de calculo y representacion grafica, con color diferente para cada valor de temperatura y concentracion
%Aqui se puede observar mediante las dos figuras los perfiles de concentracion y temperatura que se obtuvieron
%gracias a la solucion de las ecuaciones diferenciales mediante un sistema de ODE45.   
for i = 1:1:size(C_inicial,2)
	
    for j = 1:1:size(T_inicial,2)

        [t(:,:,j,i), C_reactive(:,:,j,i)] = ode45(@Diff_System,t_span,[C_inicial(i); T_inicial(j)]); %ODE System Solver
        figure(1)
        plot (t(:,:,j,i)/60, C_reactive(:,1,j,i), Color_Selection(j)); hold on; %Grafica perfiles de concentracion
        figure(2)
        plot (t(:,:,j,i)/60, C_reactive(:,2,j,i), Color_Selection(i)); hold on; %Grafica perfiles de temperatura

    end
    
    %Ajustes de la representacion grafica
    figure(1) %Grafica perfiles de concentracion
    legend('298 K','308 K','318 K','328 K');
    xlabel('Tiempo [min]');
    ylabel('Concentración [Kmol/m^3]');
    figure(2) %Grafica perfiles de temperatura
    axis ([0 5*60 275 460]) %Limites de los ejes x y y, para mayor claridad
    xlabel('Tiempo [min]');
    ylabel('Temperatura [K]');
    %Esta funcion legend es para ubicar las etiquetas de cada concentracion o temperatura en las graficas con su
    %respectivo color.
    legend([plot(t(:,:,1,1)/60, C_reactive(:,2,1,1), Color_Selection(1)) plot(t(:,:,1,2)/60, C_reactive(:,2,1,2), Color_Selection(2)) plot(t(:,:,1,3)/60, C_reactive(:,2,1,3), Color_Selection(3))],...
    'C_R(t=0) = 0 kmol/m^3','C_R(t=0) = 1 kmol/m^3','C_R(t=0) = 2 kmol/m^3','Location','northeast')

end
    
       

	