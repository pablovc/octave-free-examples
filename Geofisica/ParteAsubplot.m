 
 clf;
 %dominio para funciones senoidales
 t = linspace (0,32*16, 512);

 func= sin (((2*pi)/32)*t);

  TraFou=fft(func)

  AntiF=ifft(TraFou)

%#####SUBPLOTEOS#####

 subplot (2, 2, 1);
 RealFourier=real(TraFou)
 scatter(t,RealFourier,"filled");
title ({"Parte Real de la transformada de Fourier"});
 %axis ("nolabel","tic");
 xlabel xlabel;
 ylabel ylabel;
  
subplot (2, 2, 2);
 ImaginaFourier=imag(TraFou)
 scatter(t,ImaginaFourier,"filled");
 
title ({"Parte Imaginaria de la transformada de Fourier"});
 %axis ("nolabel","tic");
  xlabel xlabel;
 ylabel ylabel;
 
 subplot (2, 2, 3);
 Amplitud=abs(TraFou)
 scatter(t,Amplitud,"filled");
  title ({"Amplitud de la transformada de Fourier"});
 %axis ("nolabel","tic");
  xlabel xlabel;
 ylabel ylabel;
 
 subplot (2, 2, 4);
 argumento=ImaginaFourier./RealFourier
 FaseT=atan(argumento)
 scatter(t,FaseT,"filled");
  title ({"Fase de la transformada de Fourier"});
   %axis ("nolabel","tic");
 xlabel xlabel;
 ylabel ylabel;

  subplot (3, 1, 1);
 %Amplitud=abs(TraFou)
 scatter(t,Amplitud,"filled");
  title ({"Amplitud de la transformada de fourier 2"});
 %axis ("nolabel","tic");
  xlabel xlabel;
 ylabel ylabel;
 
 subplot (3, 1, 2);
  scatter (t, func, "filled");
 hold on;
 plot (t,AntiF);
 hold off;
 %scatter(t,FaseT,"filled");
  title ({"Señal original y recuperada"});
   %axis ("nolabel","tic");
 xlabel hola;
 ylabel adios;
 
 subplot (3, 1, 3);
 diferencia=func-AntiF;
   plot (t,diferencia);
  title ({"Diferencia entre señal de entrada y recuperada"});
   %axis ("nolabel","tic");
 xlabel xlabel;
 ylabel ylabel;
 
 %hold on;
 
 

 
 
 
 
 
 
 