 clf;
 %dominio para funciones senoidales
 %t = linspace (0,32*10, 20);
 %frecuencia de muestreo
 %20 muestras por ciclo (10 ciclos)
 t = linspace (0,10*10, 200);
 func= sin (((2*pi)/10)*t);
 
  TraFou=fft(func);  
  TraFouSh=fftshift(TraFou);
  
 subplot (2, 1, 1)
 scatter (t, func, "filled")
  hold on;
 plot(t,func);
  hold on;
 
 title ({"10 seg 0.1Hz"});
   grid()
 hold off;
 
  subplot (2, 1, 2)
  scatter (t, TraFouSh, "filled")
  hold on;
  plot(t,TraFouSh);
  hold on;
  title ({"Espectro en frecuencia"});
  grid();
  hold off;
  
    t1 = linspace (0,10*10, 50);
     func1= sin (((2*pi)/10)*t1);
     
       TraFou1=fft(func1);  
  TraFouSh1=fftshift(TraFou1);
   subplot (3, 1, 1)
%    scatter (t1, func1, "filled")
 % hold on;
 %plot(t1,func1);
 % hold on;
 %title ({"10 seg 0.1Hz (2s)"});
 %  grid()
 %hold off;
    scatter (t1,TraFouSh1, "filled")
 hold on;
 plot(t1,TraFouSh1);
  hold on;
 title ({"Espectro frecuencia 10 seg 0.1Hz (2s)"});
   grid()
 hold off;
 

    t2 = linspace (0,10*10, 25);
     func2= sin (((2*pi)/10)*t2);
     
       TraFou2=fft(func2);  
  TraFouSh2=fftshift(TraFou2);

   subplot (3, 1, 2)
    scatter (t2, TraFouSh2, "filled")
    hold on;
 plot(t2,TraFouSh2);
  hold on;
 title ({"Espectro en frecuencia (4s)"});
   grid();
 hold off;
 


  %fprintf ("vector de frecuencia\n")
  %freqz (TraFouSh)
  
 %    subplot (2, 2, 3);
 %nyq=nyquist(TraFouSh);
 % plot(t,nyq);
 