%!........................................................................!
%!.................UNIVERSIDAD NACIONAL AUTONOMA DE M�XICO................!
%!.........................FACULTA DE INGENIER�A..........................!
%!.....................EXAMEN EXTRAORDINARIO 2019-1.......................!
%!....................INVERSI�N DE DATOS GEOF�SICOS.......................!
%!..................... M.C. MAURICIO NAVA FLORES.........................!
%!............... ALUMNO: PINEDA FLORES GUILLERMO JESUS...................!
%!........................................................................!

%! 2)El archivo Gz.xyz contiene datos de anomal�a de Bouguer con la 
%! presencia de un campo regional que debe ser removido. Ajustar la forma 
%! del regional con una superficie polinomial y obtener la anomal�a residual.

close all
clear
%clc
format long g

%Carga de datos y se transforman las unidades: m -> km:
a=load('P3_datos_grav.xyz');
divi=100;
x=a(:,1)/divi;
y=a(:,2)/divi;
z=a(:,3);

muestX1=10201;
muestY1=10201;
%Intervalos de muestreo en X y Y:
dx1=0.25;
dy1=dx1;
X1=0:dx1:(muestX1-1)*dx1;
Y1=0:dy1:(muestY1-1)*dy1;
%X=0:dx:(muestX)*dx;
%Y=0:dy:(muestY)*dy;




largoX1=length(X1);
largoY1=length(Y1);
largoZ=length(z);

figure
%plot3(x,y,ampRes,'ok','markerfacecolor','r','markersize',1.75), grid on
surf(X1,Y1',z), shading interp, grid on, hold on
view([-30,50])
title('\bf Anomalia de Bouguer')
xlabel('\bf X [km]')
ylabel('\bf Y [km]')
zlabel('\bf \Deltag [mGal]')



%Numero total de datos y parametros:
N=length(x);
%M=6;
M=6;


%Kernel de la inversion:
G=zeros(N,M);
G(:,1)=1;
G(:,2)=x;
G(:,3)=y;
G(:,4)=x.*y;
G(:,5)=x.^2;
G(:,6)=y.^2;

%Solucion del sistema:
mest=G'*G\(G'*z);

%Datos calculados con el modelo estimado (regional):
dest=G*mest;

%Calculo del residual:
resGz=z-dest;

%Despliegue de parametros estimados:
%fprintf('Parametros estimados (Minimos Cuadrados): \n\n')
%disp(mest);
%fprintf('\n\n')


muestX=100;
muestY=100;
%Intervalos de muestreo en X y Y:
dx=1;
dy=dx;
X=0:dx:(muestX-1)*dx;
Y=0:dy:(muestY-1)*dy;


largoX=length(X);
largoY=length(Y);



%Conversion de datos tipo xyz a matriz
%Se usa una malla que consiste en 205 datos en X, por 249 datos en Y.
GzMAT=zeros(muestY,muestX);
resGzMAT=GzMAT;
regGzMAT=GzMAT;
for j=1:muestX
	for i=1:muestY
		GzMAT(i,j)=z(i+(j-1)*muestY);
		regGzMAT(i,j)=dest(i+(j-1)*muestY);
		resGzMAT(i,j)=resGz(i+(j-1)*muestY);
	end
end

%Intervalos de muestreo en X y Y:
dx=0.25;
dy=dx;
X=0:dx:(muestX-1)*dx;
Y=0:dy:(muestY-1)*dy;

%Grafico de superficies de anomalia de Bouguer, regional y residual:
figure
surf(X,Y',GzMAT), shading interp, grid on, hold on
view([-30,50])
%view([90,0])
title('\bf Anomalia de Bouguer y Regional Estimado')
xlabel('\bf X [km]')
ylabel('\bf Y [km]')
zlabel('\bf \Deltag [mGal]')
plot3(x,y,dest,'ok','markerfacecolor','r','markersize',1.75), hold off

figure
surf(X,Y',resGzMAT), shading interp, grid on
view([-30,50])
%view([90,0])
title('\bf Anomalia Residual')
xlabel('\bf X [km]')
ylabel('\bf Y [km]')
zlabel('\bf \Deltag [mGal]')

%ampRes=abs(resGzMAT);

%resGzMAT

TraFou=fft(resGzMAT);
TraFouSh=fftshift(TraFou);
ampRes=abs(TraFouSh);

figure
%plot3(x,y,ampRes,'ok','markerfacecolor','r','markersize',1.75), grid on
surf(X,Y',ampRes), shading interp, grid on
view([-30,50])
%view([90,0])
title('\bf Espectro de amplitudes')
xlabel('\bf frecX[Hz]')
ylabel('\bf frecY[Hz]')
zlabel('\bf Amplitud')

%[b, a] = butter (n, Wc)
largoAmpRes=length(ampRes);
largoGzMat=length(resGzMAT);


pause