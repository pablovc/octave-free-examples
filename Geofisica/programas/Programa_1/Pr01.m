!........................................................................!
!.................UNIVERSIDAD NACIONAL AUTONOMA DE M�XICO................!
!.........................FACULTA DE INGENIER�A..........................!
!.....................EXAMEN EXTRAORDINARIO 2019-1.......................!
!....................INVERSI�N DE DATOS GEOF�SICOS.......................!
!..................... M.C. MAURICIO NAVA FLORES.........................!
!............... ALUMNO: PINEDA FLORES GUILLERMO JESUS...................!
!........................................................................!

! 1)El archivo zRho.txt contiene datos de un pozo somero en el Golfo de M�xico.
! Obtener los par�metros �ptimos que relacionan la densidad de sedimentos (?)
! con su profundidad de sepultamiento (z), sabiendo que esta relaci�n es
! exponencial de la forma: ?=a+b?z^c

close all
clear
clc
format long g

%Carga de datos:
a=load('zRho.txt');
z=a(:,1);
rho=a(:,2);

%Grafico exploratorio de los datos:
plot(rho,z,'.-k'), grid on
xlabel('\bf \rho')
ylabel('\bf Profundidad [m]')
set(gca,'ydir','reverse')

%Numero total de datos y parametros:
N=length(z);
M=2;

%Linealizacion: RHO = ln(rho)
%                 a = ln(m1)
%                 b = m2
%                 Z = ln(z)
%               RHO = a + b*Z => mest=[ln(m1); m2]

RHO=log(rho);
Z=log(z);

%Kernel de la inversion (linealizado):
G=zeros(N,M);
G(:,1)=1;
G(:,2)=Z;

%Solucion del sistema:
mest=(G'*G)\(G'*RHO);

%Datos calculados con el modelo linealizado estimado:
m1=exp(mest(1));
m2=mest(2);
dcalc=m1*z.^m2;

%Calidad de la solucion:
e=rho-dcalc;
E=e'*e;
var_d=E/N;
med_d=mean(e);
Std_d=sqrt(var_d);

%Despliegue de resultados en pantalla:
fprintf('\n\n\n\n Modelo estimado: rho = m1 * z ^ (m2) \n\n\n')
fprintf(' m1 = %0.9f\n m2 = %0.9f\n\n\n', m1,m2)
fprintf('\n Media de los residuales: %0.9f',med_d)
fprintf('\n Desv. Estandar de los residuales: %0.9f\n\n\n',Std_d)

%Grafico de datos observados vs. estimados:
figure(1)
hold on
plot(dcalc,z,'-r','linewidth',2.0), hold off
legend('d^{obs}','d^{est}')

pause
