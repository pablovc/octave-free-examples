%Funci�n para cargar datos y graficar del problema 4
clear all
close all
datos=load('DatosCalc.dat');
s=datos(:,1);
r=datos(:,2);
rc=datos(:,3);

semilogx(s,r,'b',s,rc,'r')
title('\bf Datos de SEV')
xlabel('\bf AB/2 [m]')
ylabel('\bf Resistividad Aparente [\Omega *m]')
legend('Datos observados','Datos Caculados',0)
grid on

