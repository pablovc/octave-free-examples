!........................................................................!
!.................UNIVERSIDAD NACIONAL AUTONOMA DE MÉXICO................!
!.........................FACULTA DE INGENIERÍA..........................!
!.....................EXAMEN EXTRAORDINARIO 2019-1.......................!
!....................INVERSIÓN DE DATOS GEOFÍSICOS.......................!
!..................... M.C. MAURICIO NAVA FLORES.........................!
!............... ALUMNO: PINEDA FLORES GUILLERMO JESUS...................!
!........................................................................!

!.............................................................................................................
! 4) El archivo ab2.dat contiene datos de un sondeo eléctrico vertical adquirido con el arreglo Schlumberger. 
! La primera columna contiene la información de la mitad de la separación entre electrodos de corriente, 
! mientras que la segunda contiene los datos de resistividad aparente. Invertir los datos considerando un 
! modelo de cuatro capas horizontales (las primeras tres no exceden 40 metros de espesor en total y la 
! última tiene espesor infinito; todas las resistividades son menores o iguales a 100 mΩ).
!.............................................................................................................

PROGRAM PR04
USE inversion
IMPLICIT NONE

!----------------------------------------------------------------------------------------------------------------------------------------
!                                                     DECLARACION DE VARIABLES
!----------------------------------------------------------------------------------------------------------------------------------------                                                                                    
CHARACTER (LEN=100) :: archivo
DOUBLE PRECISION:: eps,rms,tol
INTEGER:: I,D,J,k,N,M,ierr,NC,flag=1
DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:) :: S,RAP,dMCA,M0,F0,h,DELTA
DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:) :: G

INTERFACE
FUNCTION SEV(ab2,m,nc)
	INTEGER, intent(in):: nc
	DOUBLE PRECISION, intent(in):: ab2
	DOUBLE PRECISION, allocatable, intent(in):: m(:)
	DOUBLE PRECISION:: SEV
END FUNCTION SEV
      
FUNCTION KernelSEV(ab2,f0,m0)
	INTEGER:: i, j, nc, M, N
	DOUBLE PRECISION:: deltaM
	DOUBLE PRECISION, ALLOCATABLE, INTENT(in):: ab2(:), f0(:), m0(:)
	DOUBLE PRECISION, ALLOCATABLE:: m1(:)
	DOUBLE PRECISION, ALLOCATABLE:: KernelSEV(:,:)
END FUNCTION kernelsev
END INTERFACE
!----------------------------------------------------------------------------------------------------------------------------------------
!                                      LECTURA DE DATOS DE LOS DATOS DE RESITIVIDAD APARENTE
!----------------------------------------------------------------------------------------------------------------------------------------
archivo='ab2.dat'
OPEN(10,FILE=archivo)
!Inicializacion del contador
N=0

!Lectura de los datos y conteo
READ(10,*,IOSTAT=IERR)
DO
	READ(10,*,IOSTAT=IERR)
	IF(ierr.LT.0) EXIT 
	N=N+1
ENDDO
REWIND(10)
ALLOCATE(S(n),RAP(n))
READ(10,*,IOSTAT=IERR)
DO I=1,N
	READ(10,*,IOSTAT=IERR)S(I),RAP(i)
ENDDO
CLOSE(10)


!----------------------------------------------------------------------------------------------------------------------------------------
!                                                     		
!						    	      
!----------------------------------------------------------------------------------------------------------------------------------------
!Numero de capas
nc=4
M=2*nc-1

ALLOCATE(m0(2*nc-1),F0(N),h(n))

!Resistividades
m0(1)=105
m0(2)=12
m0(3)=50
m0(4)=10

!Espesores
m0(5)=10
m0(6)=10
m0(7)=10

tol=0.1
eps=0.05

!Clico GAuss-Newton
DO WHILE (FLAG .EQ. 1)
!Respuesta del modelo inicial:
DO i=1,N
   f0(i)=SEV(s(i),m0,nc)
ENDDO


!Vector de discrepancias
h=rap-f0


!kernel
G=kernelsev(s,f0,m0)

!Minimos cuadrados amortiguados
delta=MCA(G,H,eps)

!Ajuste
m0=m0+delta

rms=1./N*sum(h)**2
rms=sqrt(rms)
IF (rms>tol) flag=0
	

ENDDO

write(*,100)
write(*,300),m0(1),m0(5),m0(2),m0(6),m0(3),m0(7),m0(4)

!Datos calculados
open(12,file='DatosCalc.dat',status='replace',action='write')
do i=1,N
  write(12,'(f10.4,2x,f15.4,2x,f15.4)')s(i),rap(i),f0(i)
end do
close(12)

open(12,file='Rseultados.dat',status='replace',action='write')
write(12,300),m0(1),m0(5),m0(2),m0(6),m0(3),m0(7),m0(4)
close(12)

!----------------------------------------------------------------------------------------------------------------------------------------
!           		                                          Formatos
!----------------------------------------------------------------------------------------------------------------------------------------
!Formato de menu
100 format(/,/'-----------------------------------------------------------------',/,&
              '||         		     PROBLEMA 05                      ||    ',/,&
              '||             					              ||    ',/,&
              '||------------------------------------------------------------||',/,&
              '||------------------------------------------------------------||',/,/,&               
              '                 Inversion de resistividades aparentes 	',/,/,&
              '||-------------------------------------------------------------||',/,&
              '-----------------------------------------------------------------',/)

             
300 format(/,/'||-------------------------------------------------------------||',/,&
 	      '||-------------------------------------------------------------||',/,&
              '                        Modelo de 4 capas                    ',/,&
              '||             					               ||',/,&
              '||-------------------------------------------------------------||',/,&
              '||-------------------------------------------------------------||',/,& 
              '||         		    CAPA 1                             ||',/,&
              '  Resistividad [Ohm*m]= ',f10.4,/,&
              '  Espesor= ',f10.4,/,/,&
              '||-------------------------------------------------------------||',/,&
              '||-------------------------------------------------------------||',/,& 
              '||         		    CAPA 2                             ||',/,&
              '  Resistividad [Ohm*m]= ',f10.4,/,&
              '  Espesor= ',f10.4,/,/,&
              '||-------------------------------------------------------------||',/,&
              '||-------------------------------------------------------------||',/,& 
              '||         		    CAPA 3                             ||',/,&
              '  Resistividad [Ohm*m]= ',f10.4,/,&
              '  Espesor= ',f10.4,/,/,&
              '||-------------------------------------------------------------||',/,&
              '||-------------------------------------------------------------||',/,& 
              '||         		    CAPA 4                             ||',/,&
              '  Resistividad [Ohm*m]= ',f10.4,/) 

END PROGRAM
!----------------------------------------------------------------------------------------------------------------------------------------
!                                         SEV             		
!						    	      
!---------------------------------------------------------------------------------------------------------------------------------------- 
FUNCTION SEV(ab2,m,nc)


   IMPLICIT NONE
   INTEGER, INTENT(in):: nc
   INTEGER:: i, j, q
   DOUBLE PRECISION, INTENT(in):: ab2
   DOUBLE PRECISION, ALLOCATABLE, INTENT(in):: m(:)
   DOUBLE PRECISION:: rho(nc), h(nc-1), c(25), R, mm
   DOUBLE PRECISION:: SEV

   rho=m(1:nc)
   h=m(nc+1:2*nc-1)

   !Orden del filtro:
   q=13

   !ParÃ¡metro del filtro relacionado con el muestreo:
   mm=4.438

   !Coeficientes del filtro:
   c=[105., 0., -262., 0., 416., 0., -746., 0., 1605., 0., -4390.,&
      0., 13396., 0., -27841., 0., 16448., 0., 8183., 0., 2525.,     &
      0., 336., 0., 225.]
   c=c/10000.
   
   SEV=0.
   DO i=1,2*q-1
      R=rho(nc)
      DO j=nc,2,-1
         R=(R+rho(j-1)*tanh(h(j-1)/(ab2*exp(((i-1)/2.-10.)*log(10.)/mm))))/ &
           (1.+R/rho(j-1)*tanh(h(j-1)/(ab2*exp(((i-1)/2.-10.)*log(10.)/mm))))
      ENDDO
      SEV=SEV+c(i)*R
   ENDDO

   RETURN
END FUNCTION SEV



!----------------------------------------------------------------------------------------------------------------------------------------
!                                        KERNEL DEL SEV             		
!						    	      
!---------------------------------------------------------------------------------------------------------------------------------------- 
FUNCTION KernelSEV(ab2,f0,m0)


   IMPLICIT NONE
   INTEGER:: i, j, nc, M, N
   DOUBLE PRECISION:: deltaM
   DOUBLE PRECISION, ALLOCATABLE, INTENT(in):: ab2(:), f0(:), m0(:)
   DOUBLE PRECISION, ALLOCATABLE:: m1(:)
   DOUBLE PRECISION, ALLOCATABLE:: KernelSEV(:,:)

   interface
      function SEV(ab2,m,nc)
         INTEGER, intent(in):: nc
         DOUBLE PRECISION, intent(in):: ab2
         DOUBLE PRECISION, allocatable, intent(in):: m(:)
         DOUBLE PRECISION:: SEV
      end function SEV
   end interface

   !Incremento para derivaciÃ³n numÃ©rica:
   deltaM=1.e-6

   N=size(f0,1)
   M=size(m0,1)
   nc=(M+1)/2

   m1=m0

   ALLOCATE(KernelSEV(N,M))
   DO j=1,M
      m1(j)=m0(j)+deltaM
      DO i=1,N
         KernelSEV(i,j)=(SEV(ab2(i),m1,nc)-f0(i))/deltaM
      ENDDO
      m1(j)=m0(j)
   ENDDO

   RETURN
END FUNCTION KernelSEV