!........................................................................!
!.................UNIVERSIDAD NACIONAL AUTONOMA DE MÉXICO................!
!.........................FACULTA DE INGENIERÍA..........................!
!....................INVERSIÓN DE DATOS GEOFÍSICOS.......................!
!.................ALUMNO: PINEDA FLORES GUILLERMO JESUS..................!
!.............................SUBRUTINAS.................................!

MODULE INVERSION

CONTAINS

!----------------------------------------------------------------------------------------------------------------------------------------
!                                              DESCOMPOSICION LU
!---------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE descLU(n,M,L,U)
INTEGER i,j,k,s
INTEGER, INTENT(IN) :: n
DOUBLE PRECISION, dimension(:,:), ALLOCATABLE, INTENT(IN) :: M
DOUBLE PRECISION, dimension(:,:), ALLOCATABLE, INTENT(OUT) ::L, U

ALLOCATE(L(n,n), U(n,n))
!Inicializamos en 0
DO i=1,n
  DO j=1,n
    L(i,j)=0
    U(i,j)=0
  ENDDO
ENDDO

!Aplicamos el algoritmos
DO i=1,n
	L(i,i)=M(i,i)
    U(i,i)=M(i,i)
    IF (i==1) THEN
      U(i,i)=U(i,i)/L(i,i)
    ELSE
      DO s=1,i-1
        L(i,i)=L(i,i)-L(i,s)*U(s,i)
        U(i,i)=U(i,i)-L(i,s)*U(s,i)
      ENDDO
      U(i,i)=U(i,i)/L(i,i)
    ENDIF
    !Matriz U
    DO j=i+1,n
 	   IF (i==1) THEN
    	  U(i,j)=M(i,j)/L(i,i)
   		ELSE
        	U(i,j)=M(i,j)
      		DO s=1,i-1
        		U(i,j)=U(i,j)-L(i,s)*U(s,j)
      		ENDDO
      		U(i,j)=U(i,j)/L(i,i)
        ENDIF
    ENDDO
    !Matriz L
    DO k=i+1,n
 	   IF (i==1) THEN
    	  L(k,i)=M(k,i)/u(i,i)
   		ELSE
        L(k,i)=M(k,i)
      	DO s=1,i-1
        	L(k,i)=L(k,i)-L(k,s)*U(s,i)
      	ENDDO
      	L(k,i)=L(k,i)/U(i,i)
        ENDIF
    ENDDO       
ENDDO

END SUBROUTINE descLU

!----------------------------------------------------------------------------------------------------------------------------------------
!                                                          INVERSA DE UNA MATRIZ
!---------------------------------------------------------------------------------------------------------------------------------------

FUNCTION INV(M)
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE::L, U,INV
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN)::M
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: B,X, W
!DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: W(:)
DOUBLE PRECISION :: S
INTEGER:: I,J,H,K,N


n=SIZE(M,1)
allocate(L(n,n),U(n,n))
call descLU(N,M,L,U)

ALLOCATE(X(N),W(N),B(N),INV(n,n))
DO H=1,N
	B=0*B
	B(H)=1
	DO I=1,N
		S=0
		DO J=1,I-1
			s=s+L(i,j)*W(j)
		ENDDO
		W(i)=(1/L(i,i))*(B(i)-s)
	ENDDO
	DO I=N,1,-1
		S=0
		DO J=i+1,n
			s=s+U(i,j)*X(j)
		ENDDO
		X(i)=(1/U(i,i))*(W(i)-s)
	ENDDO
	INV(:,h)=X
ENDDO


END FUNCTION INV

!----------------------------------------------------------------------------------------------------------------------------------------
!                                                          MINIMOS CUADRADOS 
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION MC(G,d)
!n: número de datos
!m: número de parámetros
!G: kernel de inversion
!d: Datos observados
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, INTENT(IN)::d
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: Gtd,MC
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG
INTEGER:: n,m,i
n=size(G,1)
m=size(G,2)
ALLOCATE(IGTG(m,m))


!MUltiplicacion [G'G]^⁻1*Gtd
Gt=transpose(G)
GtG=matmul(Gt,G)
Gtd=matmul(Gt,d)
igtg=inv(GtG)


MC=matmul(IGtG,Gtd)

END FUNCTION MC

!----------------------------------------------------------------------------------------------------------------------------------------
!                                  MINIMOS CUADRADOS CON ERROR RESTRINGIDO 
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION MCE(G,F,h,d)
!n: número de datos
!m: número de parámetros
!G: kernel de inversion
!d: Datos observados
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, INTENT(IN)::d,H
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: Gtd,MCE,gh
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G,F
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG,GG
INTEGER:: n,m,i,q
n=size(G,1)
m=size(G,2)
q=size(F,1)
ALLOCATE(IGTG(m,m),GG(m+q,m+q),GH(m+q))

!MUltiplicacion [G'G]^⁻1*Gtd
Gt=transpose(G)
GtG=matmul(Gt,G)
GG=0.0*GG
GG(1:M,1:M)=GtG
GG(m+1:m+q,1:m)=F
GG(1:m,m+1:m+q)=TRANSPOSE(F)
Gtd=matmul(Gt,d)
GH=gh*0.0
GH(1:M)=Gtd
GH(M+1:M+Q)=h
igtg=inv(GG)


MCE=matmul(IGtG,Gh)

END FUNCTION MCE

!----------------------------------------------------------------------------------------------------------------------------------------
!                                                          MINIMOS CUADRADOS PESADOS
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION MCP(G,d,We)
!n: número de datos
!m: número de parámetros
!G: kernel de inversion
!d: Datos observados
!W: MAtriz de peso
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, INTENT(IN)::d,We
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: Gtd,MCP
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG
INTEGER:: n,m,i
n=size(G,1)
m=size(G,2)
ALLOCATE(IGTG(m,m))


!MUltiplicacion [G'WeG]^⁻1*G'Wed
Gt=transpose(G)
DO I=1,M
	Gt(i,:)=Gt(i,:)*We
ENDDO
GtG=matmul(Gt,G)
Gtd=matmul(Gt,d)
igtg=inv(GtG)


MCP=matmul(IGtG,Gtd)

END FUNCTION MCP

!----------------------------------------------------------------------------------------------------------------------------------------
!                                                        MINIMOS CUADRADOS AMORTIGUADOS
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION MCA(G,d,eps)
!n: número de datos
!m: número de parámetros
!G: kernel de inversion
!d: Datos observados
!eps: Epsilon

DOUBLE PRECISION, INTENT(IN) :: eps
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, INTENT(IN)::d
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: Gtd,MCA
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG,E
INTEGER:: n,m,i
n=size(G,1)
m=size(G,2)
ALLOCATE(IGTG(m,m),E(m,m))

n=size(G,1)
m=size(G,2)
E=E*0.
DO i=1,m
	E(i,i)=eps**2
ENDDO

!MUltiplicacion [G'G+eps²I]^⁻1*Gtd
Gt=transpose(G)
GtG=matmul(Gt,G)
Gtd=matmul(Gt,d)
GtG=GTG+E
igtg=inv(GtG)


MCA=matmul(IGtG,Gtd)

END FUNCTION MCA

!----------------------------------------------------------------------------------------------------------------------------------------
!                                                        MINIMOS CUADRADOS AMORTIGUADOS Y PESADOS
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION MCPA(G,d,eps,We)
!n: número de datos
!m: número de parámetros
!G: kernel de inversion
!d: Datos observados
!eps: Epsilon
!w: FActor de peso

DOUBLE PRECISION, INTENT(IN) :: eps
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, INTENT(IN)::d,We
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: Gtd,MCPA
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG,E
INTEGER:: n,m,i
n=size(G,1)
m=size(G,2)
ALLOCATE(IGTG(m,m),E(m,m))


E=E*0.
DO i=1,m
	E(i,i)=eps**2
ENDDO


!MUltiplicacion [G'WeG+eps²I]^⁻1*G'Wed
Gt=transpose(G)
DO I=1,M
	Gt(i,:)=Gt(i,:)*We
ENDDO
GtG=matmul(Gt,G)
Gtd=matmul(Gt,d)
GtG=GTG+E
igtg=inv(GtG)


MCPA=matmul(IGtG,Gtd)

END FUNCTION MCPA


!----------------------------------------------------------------------------------------------------------------------------------------
!                                                          MATRIZ DE RESOLUCION DE DATOS y DEL MODELO
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION GG(G)
!G: kernel de inversion
!d: Datos observados
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG,GG,MRD
INTEGER::N,M
n=size(G,1)
m=size(G,2)

ALLOCATE(IGTG(m,m))

!MUltiplicacion [G'G]^⁻1*Gtd
Gt=transpose(G)
GtG=matmul(Gt,G)
igtg=inv(GtG)
GG=matmul(igtg,Gt)

END FUNCTION GG


!----------------------------------------------------------------------------------------------------------------------------------------
!                                                         VARIANZA
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION VARIANZA(G,SIGMA)
!G: kernel de inversion
!d: Datos observados
!dc: DAtos calculados
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(IN) :: G
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: GtG, Gt,IGtG
DOUBLE PRECISION, INTENT(IN) :: sigma
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE:: varianza
INTEGER:: N,M
n=size(G,1)
m=size(G,2)

ALLOCATE(IGTG(m,m),varianza(m))


!MUltiplicacion [G'G]^⁻1*Gtd
Gt=transpose(G)
GtG=matmul(Gt,G)
igtg=inv(GtG)

DO I=1,M
	Varianza(i)=sqrt(sigma*igtg(i,i))
ENDDO


END FUNCTION Varianza

!----------------------------------------------------------------------------------------------------------------------------------------
!                                              KERNEL POLINOMIAL
!---------------------------------------------------------------------------------------------------------------------------------------
FUNCTION KERNEL(grado,x)
!grado: es el grado del polinomio
!x:     es el vector de datos del polinomio
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, INTENT(IN)::X
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: KERNEL
INTEGER, INTENT(IN):: grado
INTEGER :: N

N=size(x)
M=grado+1
ALLOCATE(KERNEL(n,m))
DO i=0,grado
	KERNEL(:,i+1)=x**(grado-i)
ENDDO	
END FUNCTION KERNEL
 
END MODULE INVERSION