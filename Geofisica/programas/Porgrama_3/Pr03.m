!........................................................................!
!.................UNIVERSIDAD NACIONAL AUTONOMA DE M�XICO................!
!.........................FACULTA DE INGENIER�A..........................!
!.....................EXAMEN EXTRAORDINARIO 2019-1.......................!
!....................INVERSI�N DE DATOS GEOF�SICOS.......................!
!..................... M.C. MAURICIO NAVA FLORES.........................!
!............... ALUMNO: PINEDA FLORES GUILLERMO JESUS...................!
!........................................................................!

! 3)Estimar la variaci�n diurna de los d�as 9, 10 y 11 de diciembre de 2002, 
! a partir de la intensidad de campo total (componente F) registrada en el 
! observatorio magn�tico de Tucson, Arizona, E.U.A. 
! (archivos tuc20021209dmin.min, tuc20021210dmin.min y tuc20021211dmin.min), 
! considerando que la variaci�n diurna es resultado de los datos observados 
! menos la tendencia y la tendencia se estima con una curva suave que cruza 
! exactamente sin error por el primer y �ltimo dato observado de cada d�a.

close all
clear
clc
format long g

%Carga de datos (3 archivos):
a=dlmread('tuc20021209dmin.min','',26,3);
b=dlmread('tuc20021210dmin.min','',26,3);
c=dlmread('tuc20021211dmin.min','',26,3);

%Concatenacion de datos (un solo arreglo):
A=[a;b;c];

%Numero total de datos y parametros:
N=length(A);
M=4;

%Eje de tiempo en d�as:
HR=0:1/(24*60):(N-1)/(24*60);

%---------------------------------------------------------------------------------------------------
%	INVERSIoN DE LA TENDENCIA GENERAL POR MINIMOS CUADRADOS AMORTIGUADOS
%---------------------------------------------------------------------------------------------------
%Kernel de la inversiOn (polinomio de orden M-1):
G=zeros(N,M);
for i=1:M
  G(:,i)=HR.^(i-1);
end

%SoluciOn del sistema:
eps2=0.01;
mest=(G'*G+eps2*eye(M))\(G'*A(:,4));

%Datos calculados con el modelo estimado:
dest=G*mest;

%Despliegue de parámetros estimados:
fprintf('Parametros estimados (Minimos Cuadrados Amortiguados): \n\n')
disp(mest);
fprintf('\n\n\n')
%---------------------------------------------------------------------------------------------------

plot(HR,A(:,4),'-k','linewidth',1.75), grid minor, hold on
plot(HR,dest,'-b','linewidth',1.75)
title('\bf Intensidad de Campo Total observada en el IMO TUC')
xlabel('\bf Dias consecutivo de observacion')
ylabel('\bf Componente F [nT]')
legend('d_{obs}','d_{Min C}','location','southwest')
pause

%---------------------------------------------------------------------------------------------------
%	INVERSION DE LA TENDENCIA GENERAL POR MINIMOS CUADRADOS CON ERROR RESTRINGIDO
%---------------------------------------------------------------------------------------------------
%Puntos por los que debe pasar la curva (t,h):
t=[HR(1);1;2;HR(N)];
h=[0;0;0;0];
for i=1:4
	for j=1:N
		if HR(j)==t(i)
			h(i)=A(j,4);
		end
	end
end

%Operador F (Fm=h):
F=zeros(4,M);
for i=1:4
	for j=1:M
		F(i,j)=t(i)^(j-1);
	end
end

%Arreglos "aumentados" para la inversion por error con restriccion:
eps2=0.001;
GG=[G'*G, F'; F, zeros(4)]+eps2*eye(size(GG,1));
D=[G'*A(:,4);h];

%Solucion del sistema:
MEST=GG\D;

%Datos calculados con el modelo estimado:
DEST=G*MEST(1:M);

%Despliegue de parametros estimados:
fprintf('Parametros estimados (Minimos Cuadrados con Error Restringido): \n\n')
disp(MEST(1:M));
fprintf('\n\n\n')

%Figura comparativa: datos observados Vs. calculados por ambos metodos de inversion:
plot(HR,A(:,4),'-k','linewidth',1.75), grid minor, hold on
plot(HR,dest,'-b','linewidth',1.75)
plot(HR,DEST,'-r','linewidth',1.75), hold off
title('\bf Intensidad de Campo Total observada en el IMO TUC')
xlabel('\bf Dias consecutivo de observacion')
ylabel('\bf Componente F [nT]')
legend('d_{obs}','d_{Min C}','d_{Min C Err R}','location','southwest')
%---------------------------------------------------------------------------------------------------

%---------------------------------------------------------------------------------------------------
%	ESTIMACION DE LA VARIACION DIURNA (Residual: datos observados menos tendencia)
%---------------------------------------------------------------------------------------------------
VD1=A(:,4)-dest;
VD2=A(:,4)-DEST;

%Figura comparativa: Variacion Diurna estimada por los dos metodos de inversion:
figure
plot(HR,VD1,'-b','linewidth',1.75), grid minor, hold on
plot(HR,VD2,'-r','linewidth',1.75), hold off
title('\bf Variacion Diurna Estimada para el IMO TUC')
xlabel('\bf Dias consecutivo de observacion')
ylabel('\bf Componente F [nT]')
legend('VD_{Min C}','VD_{Min C Err R}','location','southwest')
pause