%!........................................................................!
%!.................UNIVERSIDAD NACIONAL AUTONOMA DE M�XICO................!
%!.........................FACULTA DE INGENIER�A..........................!
%!.....................EXAMEN EXTRAORDINARIO 2019-1.......................!
%!....................INVERSI�N DE DATOS GEOF�SICOS.......................!
%!..................... M.C. MAURICIO NAVA FLORES.........................!
%!............... ALUMNO: PINEDA FLORES GUILLERMO JESUS...................!
%!........................................................................!

%! 2)El archivo Gz.xyz contiene datos de anomal�a de Bouguer con la 
%! presencia de un campo regional que debe ser removido. Ajustar la forma 
%! del regional con una superficie polinomial y obtener la anomal�a residual.

close all
clear
%clc
format long g

%Carga de datos y se transforman las unidades: m -> km:
a=load('Gz.xyz');
x=a(:,1)/1000;
y=a(:,2)/1000;
z=a(:,3);

%Grafico exploratorio de los datos:
plot3(x,y,z,'ok','markerfacecolor','r','markersize',1.75), grid on
title('\bf Anomalia de Bouguer')
xlabel('\bf X [km]')
ylabel('\bf Y [km]')
zlabel('\bf \Deltag [mGal]')

%Numero total de datos y parametros:
N=length(x);
M=6;

%Kernel de la inversion:
G=zeros(N,M);
G(:,1)=1;
G(:,2)=x;
G(:,3)=y;
G(:,4)=x.*y;
G(:,5)=x.^2;
G(:,6)=y.^2;

%Solucion del sistema:
mest=G'*G\(G'*z);

%Datos calculados con el modelo estimado (regional):
dest=G*mest;

%Calculo del residual:
resGz=z-dest;

%Despliegue de parametros estimados:
fprintf('Parametros estimados (Minimos Cuadrados): \n\n')
disp(mest);
fprintf('\n\n')

%Conversion de datos tipo xyz a matriz
%Se usa una malla que consiste en 205 datos en X, por 249 datos en Y.
GzMAT=zeros(249,205);
resGzMAT=GzMAT;
regGzMAT=GzMAT;
for j=1:205
	for i=1:249
		GzMAT(i,j)=z(i+(j-1)*249);
		regGzMAT(i,j)=dest(i+(j-1)*249);
		resGzMAT(i,j)=resGz(i+(j-1)*249);
	end
end

%Intervalos de muestreo en X y Y:
dx=0.25;
dy=dx;
X=0:dx:204*dx;
Y=0:dy:248*dy;

%Grafico de superficies de anomalia de Bouguer, regional y residual:
figure
surf(X,Y',GzMAT), shading interp, grid on, hold on
view([-30,50])
title('\bf Anomalia de Bouguer y Regional Estimado')
xlabel('\bf X [km]')
ylabel('\bf Y [km]')
zlabel('\bf \Deltag [mGal]')
plot3(x,y,dest,'ok','markerfacecolor','r','markersize',1.75), hold off

figure
surf(X,Y',resGzMAT), shading interp, grid on
view([-30,50])
title('\bf Anomalia Residual')
xlabel('\bf X [km]')
ylabel('\bf Y [km]')
zlabel('\bf \Deltag [mGal]')

pause