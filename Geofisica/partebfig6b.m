  
 clf;

  t2 = linspace (0,(10*10), 25);
     func2= sin (((2*pi)/10)*t2);
     
       TraFou2=fft(func2);  
  TraFouSh2=fftshift(TraFou2);

   subplot (3, 1, 2)
    scatter (t2, TraFouSh2, "filled")
    hold on;
 plot(t2,TraFouSh2);
  hold on;
 title ({"Espectro en frecuencia (4s)"});
   grid();
 hold off;
 

    t3 = linspace (0,(10*10)+4, 12.5);
     func3= sin (((2*pi)/10)*t3);
     
       TraFou3=fft(func3);  
  TraFouSh3=fftshift(TraFou3);

   subplot (3, 1, 2)
    scatter (t3, TraFouSh3, "filled")
    hold on;
 plot(t3,TraFouSh3);
  hold on;
 title ({"Espectro en frecuencia (8s)"});
   grid();
 hold off;
 

  %fprintf ("vector de frecuencia\n")
  %freqz (TraFouSh)
  
 %    subplot (2, 2, 3);
 %nyq=nyquist(TraFouSh);
 % plot(t,nyq);
