 clf;
 %dominio para funciones senoidales
 %t = linspace (0,32*10, 20);
 %frecuencia de muestreo
 %20 muestras por ciclo (10 ciclos)
 t0 = linspace (0,40, 200);
 senoide0= sin (((2*pi)/10)*t0);
 cosenoide0=4*cos(((2*pi)/25)*t0);
 %plot(t0,senoide0);
  %hold on;
 %plot(t0,cosenoide0);
 % hold on;

% title ({"Prueba Señales"});
 %  grid()
 %hold off;
 
 %Se piden 100 datos
 t= linspace (0,10*10, 100);
 senoide= sin (((2*pi)/10)*t);
 cosenoide=4*cos(((2*pi)/25)*t);
 sumaSC=senoide+cosenoide;
 
 subplot(2,1,1)
%seno
 plot(t,senoide);
 hold on;
 %coseno
 plot(t,cosenoide);
 hold on;

 %suma de señales
  scatter(t,sumaSC,"filled");
  hold on;
 plot(t,sumaSC);
  hold on;

 title ({"Señal"});
   grid()
 hold off;
 
 
  TraFou=fft(sumaSC);  
  TraFouSh=fftshift(TraFou);
 
 
 subplot(2,1,2)
  scatter(t,TraFouSh,"filled");
  hold on;
 plot(t,TraFouSh);
  hold on;
 title ({"Espectro en frecuencia"});
   grid()
 hold off;
 
 rndm=rand(100,1);
 
 %rndm10=rand(100,10);
 
 sumaAlet1=sumaSC+rndm;
 
 sumaAlet10=sumaSC+(rndm*10);
 
 
 % fprintf ("tamaño suma SC\n")
 %length(sumaSC)
 
 % fprintf ("tamaño suma Random1\n")
 %length(rndm1)
 

 %fprintf ("tamaño suma Random 10\n")
 %length(rndm10)
 
 
 % fprintf ("tamaño suma sumaSC\n")
 %length(sumaSC)
  
 subplot(4,1,1)
 plot(t,sumaAlet);
  hold on;
 title ({"Señal con ruido escala 1"});
   grid()
 hold off;
  
  subplot(4,1,2)
 plot(t,sumaAlet10);
  hold on;
 title ({"Señal con ruido escala 10"});
   grid()
 hold off;
 

 
 
 
 