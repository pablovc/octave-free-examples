   clf;
 %dominio para funciones senoidales
 t = linspace (0,32*16, 512);
 t1 = linspace (0,(32*16)-1, 511);
frec=100;
  func=sin(((2*pi)/32)*t);
   derSeno=diff(func);
   
   derSenoFourier=abs(fft(func)*(2*pi*frec*i));
   
   AntiFourier=ifft(derSenoFourier);
  %plot(t1,derSeno)
  %title ({"Derivada de seno"});
 % hold on;
  
%MUY UTIL PARA DEBBUGING
%largoDerSeno= length (derSeno) 
%largoT1= length (t1)

plot(t, AntiFourier);
title ({"Derivada de seno Fourier"});
 
 