   clf;
 %dominio para funciones senoidales
 t = linspace (0,32*16, 512);

  func= sin (((2*pi)/32)*t);
 % plot (t,func);
 %hold on;
  funcTiempo= sin (((2*pi)/32)*t-((8*2*pi)/32));
 % plot (t,funcTiempo);
 % hold off;
  
  TraFou1=fft(func);
  
  TraFou2=fft(funcTiempo);
  
  AntiF1=ifft(TraFou1);
  
  AntiF2=ifft(TraFou2);
  
  plot (t,AntiF1);
   hold on;
  scatter (t,AntiF2,"filled");
  hold on;
  diferencia1=func- AntiF1;
  diferencia2=funcTiempo- AntiF2;
   plot (t,diferencia2);
  
  title ({"Señal desplazada recuperada"});
   legend ({"señal original", "señal recuperad"}, "location", "east");
  hold off;
  
  desp=abs(diferencia2)-abs(diferencia1)
  
 
 %hold on;
 %Montessorimente
 %t = linspace (0,2*pi, 100);

  %funcTiempo= sin ((pi)*t+(pi/4));
  %plot (t,funcTiempo);
 