 clf;
 %dominio para funciones senoidales
 %t = linspace (0,32*10, 20);
 periodo=10;
 fMuestreo=2;
 t = linspace (0,periodo, fMuestreo*periodo);
%frec=2;
ciclos=10;
 func= sin (((2*pi)*ciclos)*t);
 
 %Df de la frecuencia
Df=1/fMuestreo; 
%Limite para el Vector
LV=ciclos*periodo; 
%Freq. de Nyquist
FNyQ=1/(2*Df); 
%Frecuencia Fundamental
Ff=1/periodo;
 
 %scatter (t, func, "filled")
 %plot(t,func);
 
  TraFou=fft(func);
  
   
  TraFouSh=fftshift(TraFou);
  
 subplot (2, 1, 1)
 %scatter (t, func, "filled")
 plot(t,func);
  hold on;
 
 title ({"10 seg 10Hz"});
   grid()
 hold off;
 
  subplot (2, 1, 2)
  % scatter (t, TraFouSh, "filled")
 plot(t,TraFouSh);
  hold on;
 
 title ({"Espectro en frecuencia"});
   grid()
 hold off;

  %fprintf ("vector de frecuencia\n")
  %freqz (TraFouSh)
  
 %    subplot (2, 2, 3);
 %nyq=nyquist(TraFouSh);
 % plot(t,nyq);
 