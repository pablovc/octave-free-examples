%Parte B. Proyecto 2
clear all;
close all;

%ejercicio 5------------------------------------------------------------
c=10; %ciclos
T=10; %periodo [s]
fs=2; %frecuencia de muestreo [Hz]
dt=1/fs; %icremento en la frecuencia
p=c*T; %limite de vector
fn=1/(2*dt); %frecuencia de nyquist
f=1/T; %frecuencia fundamental
t=0:0.5025:p;
%se�al
x=sin(2*pi*t/10); %se�al senoidal

%a)
figure (1)
subplot(2,1,1);
plot(t,x,'b');
xlim([0 100])
ylim([-2 2])
title 'Funci�n senoidal- 10 ciclos'
xlabel 'Tiempo [s]'
ylabel 'Amplitud'

%b)
X=fft(x); %transformada de Fourier se la se�al
Xam=abs(X); %Espectro de amplitud
Xamno=Xam/max(Xam); %Espectro de amplitud normalizado
frec=-fn:2/199:fn; %vector de frecuencias
subplot(2,1,2);
plot(frec,Xamno,'g');
xlim([0 1])
ylim([-1 2])
title 'Espectro de amplitud'
xlabel 'Frecuencia [Hz]'
ylabel '|X(f)|'

%ejercicio 6-------------------------------------------------------------
t2=0:100/99:100;
x2=sin(2*pi*t2/10);
X2=fft(x2); %transformada de Fourier se la se�al
X2am=abs(X2); %Espectro de amplitud
X2amno=X2am/max(X2am); %Espectro de amplitud normalizado

%a)
dta=2; %Incremento de frecuencia a
fma=1/dta; %frec de muestreo a
fna=1/(2*dta); %frec de nyquist a
freca=-fna:0.5/99:fna; %vector de frecuencias para a)

figure (2)
subplot(3,1,1);
plot(freca,X2amno,'r');
title 'a) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-0.5 1.5])

%b)
dtb=4; %Incremento de frecuencia b
fmb=1/dtb;  %frec de muestreo b
fnb=1/(2*dtb); %frec de nyquist b
frecb=-fnb:0.25/99:fnb; %vector de frecuencias para b)

subplot(3,1,2);
plot(frecb,X2amno,'g');
title 'b) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-0.5 1.5])

%c)
dtc=8; %Incremento de frecuencia c
fmc=1/dtc;%frec de muestreo c
fnc=1/(2*dtc); %frec de nyquist c
frecc=-fnc:0.125/99:fnc; %vector de frecuencias para c)

subplot(3,1,3);
plot(frecc,X2amno,'b');
title 'c) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-0.5 1.5])

%EJERCICIO 7-----------------------------------------------------------

%a)
FS=1; %frec de muestreo
F=1/T; %frec fundamental
DT=1/FS; %incremento en la frecuencia
FN=1/(2/DT); %frec de nyquist
FREC=-FN:1/99:FN; %vector de frecuencias
t3=0:100/99:100;

%se�ales
x=sin(2*pi*t3/10);
x3=4*cos(2*pi*t3/25);
%Suma de se�ales
sx=x+x3;

for z=1:100 %creaci�n de vector de ruido
    r(z)=rand; %asignaci�n de valores de ruido
end
ra=10*r; %ruido amplificado

%Suma del ruido a la suma de se�ales
sxr=sx+r; %sumando ruido normal
sxra=sx+ra; %sumando ruido amplificado

figure (3)
subplot(2,1,1);
plot(t3,x,'g')
hold on;
plot(t3,x3,'b')
hold on;
plot(t3,sx,'r')
xlabel 'Tiempo [s]' 
ylabel 'Amplitud' 
title 'Se�ales'
legend('Sin', 'Cos', 'Suma de se�ales');
ylim([-6 6])

%b)
%Transformadas de Fourier
SX=fft(sx); %Suma de se�ales
SXR=fft(sxr); %suma de se�ales mas ruido
SXRA=fft(sxra); %Suma de se�ales mas ruido amplificado

%Espectros de amplitud
EsAmpSX=abs(SX); %Suma de se�ales
EsAmpSXR=abs(SXR); %suma de se�ales mas ruido
EsAmpSXRA=abs(SXRA); %Suma de se�ales mas ruido amplificado

%Espectros de Amplitud Normalizados
EsAmpSXnor=EsAmpSX/max(EsAmpSX);%Suma de se�ales
EsAmpSXRnor=EsAmpSXR/max(EsAmpSXR); %suma de se�ales mas ruido
EsAmpSXRAnor=EsAmpSXRA/max(EsAmpSXRA); %Suma de se�ales mas ruido amplificado

subplot(2,1,2);
plot(FREC,EsAmpSXnor); 
xlabel 'Frecuencia [Hz]'
ylabel '|X(f)|'
title 'Espectro de amplitud'
xlim([0 0.5])
ylim([-0.5 1.5])

%c)
figure (4)
subplot(4,1,1);
plot(t3,sxr,'r');
xlabel 'Tiempo [s]'
ylabel 'Amplitud'
title 'Se�al mas ruido'
    
subplot(4,1,2);
plot(FREC,EsAmpSXRnor);
xlabel 'Frecuencia [Hz]'
ylabel '|X(f)|' 
title 'Espectro de amplitud de la se�al m�s ruido'  
xlim([0 0.5])
    
%d)
subplot(4,1,3);
plot(t3,sx, 'g');
hold on;
plot(t3,ra,'r');
hold on;
plot(t3,sxra,'b');
legend('Funci�n Suma', 'Ruido Amplificado', 'Funci�n con Ruido'); 
xlabel 'Tiempo [s]'
ylabel 'Amplitud'
title 'Se�ales'
    
subplot(4,1,4);
plot(FREC,EsAmpSXRAnor);
xlabel 'Frecuencia [Hz]' 
ylabel '|X(f)|'
title 'Espectro de amplitud de la se�al m�s el ruido amplificado'
xlim([0 0.5])










