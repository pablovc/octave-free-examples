%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% PROYECTO 2 PGyM %%%%%%%%%%%%%
%%%%%%%%%%     PARTE B     %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;
close all;

%%%%%%%%%%%%%%%%%%%%%% Ejercicio 5 %%%%%%%%%%%%%%%%%%%%%%
%Numero de Ciclos
Nc=10; 
%Periodo (En segundos)
Ps=10; 
%Frecuencia de Muestreo (En Hz)
Fs=2; 
%Df de la frecuencia
Df=1/Fs; 
%Limite para el Vector
LV=Nc*Ps; 
%Freq. de Nyquist
Fn7=1/(2*Df); 
%Frecuencia Fundamental
Ff=1/Ps;
t=0:0.5025:LV;
%se�al Senoidal
x=sin(2*pi*t/10); 

%Inciso A
figure (1)
subplot(2,1,1);
plot(t,x,'r');
xlim([0 100])
ylim([-2 2])
title 'Funci�n senoidal a 10 ciclos'
xlabel 'Tiempo [s]'
ylabel 'Amplitud'
grid on

%Inciso B
%Transformda de Fourier de la se�al
X=fft(x); 
%Espectro de amplitud de la se�al
Xamp=abs(X); 
%Aqui normalizamos el espectro de amplitud (Xena= Espectro normalizado de
%amplitud
Xena=Xamp/max(Xamp); 
%Vector de frecuencias
Vfrq=-Fn7:2/199:Fn7; 
subplot(2,1,2);
plot(Vfrq,Xena,'b');
xlim([0 1])
ylim([-1 2])
title 'Espectro de amplitud'
xlabel 'Frecuencia [Hz]'
ylabel '|X(f)|'
grid on
%%%%%%%%%%%%%%%%%%%%%% Ejercicio 6 %%%%%%%%%%%%%%%%%%%%%%
t2=0:100/99:100;
x2=sin(2*pi*t2/10);
%Transformada de Forier de la se�al
X2=fft(x2); 
%Espectro de amplitud de a se�al
X2eas=abs(X2); 
%Espectro normalizado de amplitud
X2ena=X2eas/max(X2eas); 

%Inciso A
%Df de la frecuencia
DtA=2; 
%Frecuencia de muestreo A
FmA=1/DtA; 
%Frecuencia de Nyquist A
FNA=1/(2*DtA); 
%Vector de frecuencias para A
VFRA=-FNA:0.5/99:FNA;

figure (2)
subplot(3,1,1);
plot(VFRA,X2ena,'y');
title 'a) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-1.5 1.5])
grid on
%Inciso B
%Df para B
DtB=4; 
%Frecuencia de muestreo para B
FmB=1/DtB; 
%Frecuencia de Nyquist para B 
FNb=1/(2*DtB);
%Vector de frecuencias para B
VfrqB=-FNb:0.25/99:FNb; 

subplot(3,1,2);
plot(VfrqB,X2ena,'g');
title 'b) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-0.5 1.5])
grid on
%Inciso C
%Df para C
DtC=8; 
%Frecuencia de muestreo para C
FmC=1/DtC;
%Frecuencia de Nyquist para C
FNC=1/(2*DtC); 
% Vector de frecuencias para C
VfrqC=-FNC:0.125/99:FNC; 

subplot(3,1,3);
plot(VfrqC,X2ena,'b');
title 'c) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-1.5 1.5])
grid on
%%%%%%%%%%%%%%%%%%%%%% Ejercicio 7 %%%%%%%%%%%%%%%%%%%%%%
%Inciso A
% Frecuencia de muestreo
FMA7=1;
% Frecuencia fundamental
F=1/Ps; 
%Df para A
DT=1/FMA7; 
% Frecuencia de Nyquist
Fn7=1/(2/DT); 
%Vector de Frecuencias
FREC=-Fn7:1/99:Fn7;
t3=0:100/99:100;

%Se�ales para sumar
x=sin(2*pi*t3/10);
x3=4*cos(2*pi*t3/25);
%Suma de se�ales
SSX=x+x3;

for z=1:100 
%Vector de ruido
    r(z)=rand; 
%Valores de ruido
end
RA=10*r; 
%Ruido amplificado

%Suma del ruido 
SxR=SSX+r; 
%Sumando ruido amplificado
Sxra=SSX+RA;

figure (3)
subplot(2,1,1);
plot(t3,x,'g')
hold on;
plot(t3,x3,'b')
hold on;
plot(t3,SSX,'r')
xlabel 'Tiempo [s]' 
ylabel 'Amplitud' 
title 'Se�ales'
legend('Sin', 'Cos', 'Suma de se�ales');
ylim([-6 6])
grid on
%Inciso B
%Transformadas de Fourier
%Suma de Se�ales
SX=fft(SSX); 
%Suma de se�ales mas el ruido
SXR=fft(SxR); 
%Suma de se�ales mas el ruido amplificado
SXRA=fft(Sxra); 

%Espectros de amplitud
%Suma de se�ales
EsAmpSX=abs(SX); 
%Suma de se�ales mas ruido
EsAmpSXR=abs(SXR); 
%Suma de se�ales mas el ruido amplificado
EsAmpSXRA=abs(SXRA); 

%Espectros de Amplitud Normalizados
%Suma de se�ales
EsAmpSXnor=EsAmpSX/max(EsAmpSX);
%Suma de se�ales mas el ruido
EsAmpSXRnor=EsAmpSXR/max(EsAmpSXR); 
%Suma de se�ales mas el ruido amplificado
EsAmpSXRAnor=EsAmpSXRA/max(EsAmpSXRA);

subplot(2,1,2);
plot(FREC,EsAmpSXnor); 
xlabel 'Frecuencia [Hz]'
ylabel '|X(f)|'
title 'Espectro de amplitud'
xlim([0 0.5])
ylim([-0.5 1.5])
grid on
%Inciso C
figure (4)
subplot(4,1,1);
plot(t3,SxR,'r');
xlabel 'Tiempo [s]'
ylabel 'Amplitud'
title 'Se�al mas ruido'
 grid on   
subplot(4,1,2);
plot(FREC,EsAmpSXRnor);
xlabel 'Frecuencia [Hz]'
ylabel '|X(f)|' 
title 'Espectro de amplitud de la se�al m�s ruido'  
xlim([0 0.5])
grid on    
%Inciso D
subplot(4,1,3);
plot(t3,SSX, 'g');
hold on;
plot(t3,RA,'r');
hold on;
plot(t3,Sxra,'b');
legend('Funci�n Suma', 'Ruido Amplificado', 'Funci�n con Ruido'); 
xlabel 'Tiempo [s]'
ylabel 'Amplitud'
title 'Se�ales'
    
subplot(4,1,4);
plot(FREC,EsAmpSXRAnor);
xlabel 'Frecuencia [Hz]' 
ylabel '|X(f)|'
title 'Espectro de amplitud de la se�al m�s el ruido amplificado'
xlim([0 0.5])
grid on









