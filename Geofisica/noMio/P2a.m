%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% PROYECTO 2 PGyM %%%%%%%%%%%%%
%%%%%%%%%%     PARTE A     %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
clc
%Calculamos que la longitud de la onda senoidal a 32 metros y 512 puntos
K=(2*pi)/32;
%Creamos un vector que servir� para guardar la se�al base 
f=1:32;
%Creamos el vector que ser� nuestra se�al de 512 puntos
Fx=1:512;
G=1;
%Creamos la iteraci�n para repetir nuestra se�al
for c=1:16
   %Se�al base
for r=1:32
f(r)=sin(K*(r-1));
Fx(G)=f(r);
G=G+1;
end
end
x=0:511
%Grafica de la se�al sintetica
figure(1)
grid on
hold on
plot(x,Fx)
xlabel('distancia [m]')
ylabel('amplitud [m]')
title('se�al sintetica','FontSize',12)

xx=1:257;
%transformada de fourier de la se�al
y=fft(Fx);
iy=ifft(y);
%Espectro bilateral a 512
P2 = abs(y/512);
figure(2)
grid on
hold on
plot(x,P2)
title('espectro bilateral)')
xlabel('f (Hz)')
ylabel('|P2(f)|')

figure (2);
%Grafica de la parte real de la transformada de fourier
subplot(2,2,1) 
grid on
hold on
plot(x,real(y))
xlabel('Distancia [m]')
ylabel('Dmplitud [m]')
title('Parte real TF')

subplot(2,2,2) 
%Parte imaginaria de la transformada de Fourier.
grid on
hold on
plot(imag(y))
xlabel('distancia [m]')
ylabel('amplitud [m]')
title('parte imaginaria TF')
%La amplitud de la transformada de Fourier.
subplot(2,2,3) 
grid on
hold on
plot(x,P2)
title('espectro bilateral)')
xlabel('f (Hz)')
ylabel('|P2(f)|')
%Fase en radianes
subplot(2,2,4) 
grid on
hold on
plot(x,Fx)
xlabel('distancia [m]')
ylabel('amplit')
title('fase')


P1 = P2(1:length(y)/2+1);
%Espectro unilateral
P1(2:end-1) = 2*P1(2:end-1);
figure(3)
%Amplitud de la transformada de Fourier
subplot(3,1,1) 
plot(xx,P1)
hold on
title('espectro unilateral)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
%Sse�al de entrada original y la se�al recuperada (Se�al original con
%puntos)
subplot(3,1,2) 
grid on
hold on
plot(x,Fx,'r.')
plot(x,iy)
xlabel('distancia [m]')
ylabel('amplitud [m]')
legend('se�al original','se�al recuperada')
title('se�ales')


%Desplazamiento
%creaci�n de la se�al desplazada
m=1;
for c=1:16
for r=1:32
s(r)=sin(K*(r-1));
(m)==s(r);
m=m+1;
end
end
figure(4);
grid on
hold on
%Se�al desplaada con Fourier (sabemos que una se�al desplazda ser� igual a:
% (T.F)*e^-j*2*pi*f*t0)
w=(16*pi);
SDf= y*exp(w*((-1)^(1/2)));   %se�al desplazada Fourier
Sd=ifft(SDf)
figure(5);
grid on
hold on
plot(8:519,Sd)
xlabel('metros')
ylabel('metros')
title('se�al desplazada','FontSize',12)

%Derivada
%Creamos una iteraci�n para repetir nuestra se�al
G=1;
for c=1:16
for r=1:32
D(r)=(-1)*cos(K*(r-1));
Dx(G)=D(r);
G=G+1;
end
end
plot(x,Dx)
title('espectro unilateral)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
grid on
for l=1:512
DFO(l)=ifft(y(l)*(2*pi*((-1)^(1/2)*l/32)));
end

figure(6);
grid on
hold on
plot(x,DFO)
xlabel('metros')
ylabel('metros')
title('Derivada','FontSize',12)