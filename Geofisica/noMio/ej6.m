t2=0:100/99:100;
x2=sin(2*pi*t2/10);
%Transformada de Forier de la se�al
X2=fft(x2); 
%Espectro de amplitud de a se�al
X2eas=abs(X2); 
%Espectro normalizado de amplitud
X2ena=X2eas/max(X2eas); 

%Inciso A
%Df de la frecuencia
DtA=2; 
%Frecuencia de muestreo A
FmA=1/DtA; 
%Frecuencia de Nyquist A
FNA=1/(2*DtA); 
%Vector de frecuencias para A
VFRA=-FNA:0.5/99:FNA;

figure (2)
subplot(3,1,1);
plot(VFRA,X2ena,'c');
title 'a) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-1.5 1.5])
grid on
%Inciso B
%Df para B
DtB=4; 
%Frecuencia de muestreo para B
FmB=1/DtB; 
%Frecuencia de Nyquist para B 
FNb=1/(2*DtB);
%Vector de frecuencias para B
VfrqB=-FNb:0.25/99:FNb; 

subplot(3,1,2);
plot(VfrqB,X2ena,'r');
title 'b) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-0.5 1.5])
grid on
%Inciso C
%Df para C
DtC=8; 
%Frecuencia de muestreo para C
FmC=1/DtC;
%Frecuencia de Nyquist para C
FNC=1/(2*DtC); 
% Vector de frecuencias para C
VfrqC=-FNC:0.125/99:FNC; 

subplot(3,1,3);
plot(VfrqC,X2ena,'g');
title 'c) Espectro de amplitud'
xlabel('Frecuencia [Hz]'); 
ylabel('|X(f)|');
xlim([-0.25 0.25])
ylim([-1.5 1.5])
grid on
